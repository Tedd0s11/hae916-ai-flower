from os import supports_effective_ids
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
from pandas.core.base import DataError
from sklearn import svm
import seaborn as sb

def feel_data():
    data = pd.read_csv("flowerTrain.data",names = ['sepal length','sepal width','petal length','petal width','species'])
    x = data.iloc[:,0:4]
    print(np.shape(x))
    plt.figure()
    plt.scatter(data.iloc[:,0],data.iloc[:,1],alpha =0.2,s=100*data.iloc[:,2],c=100*data.iloc[:,3])
    plt.xlabel("Sepal Length",fontsize=16)
    plt.xticks(fontsize = 15)
    plt.yticks(fontsize = 15)
    plt.ylabel("Sepal Width",fontsize=16)
    plt.title("Feeling of Data",fontsize=16)
    # plt.legend(['Step = 1','Step = 10','Step = 0.1'],fontsize=16)
    plt.show()
    
    plt.show()
    return x,data

def PCA(X, reducedDim):
    mean = np.mean(X)
    X_shift = X - mean
    C = np.cov(X_shift)
    # C = X_shift@np.transpose(X_shift)
    eigen_value,eigen_vector = np.linalg.eigh(C)
    p = max(eigen_value)
    Up = eigen_vector - p
    s = np.transpose(Up)@X_shift
    
    print("Mean of X :\n",mean)
    print("X_shifted :\n",X_shift)
    print("Covariance of X: \n",C)
    print("Eigenvalue :\n",eigen_value)
    print("Eigenvector :\n",eigen_vector)
    print("Up :\n",Up)
    print("largest eigenvalue :\n",p)
    print("s :",s)
    
    origin = [-0.5,0.5]
    eig_vec1 = eigen_vector[:,0]
    eig_vec2 = eigen_vector[:,1]



    plt.figure()
    plt.quiver(*origin, *eig_vec1, color=['r'], scale=21)
    plt.quiver(*origin, *eig_vec2, color=['b'], scale=21)
    # plt.arrow(Up[:,0],Up[:,1],1,1)
    plt.scatter(X[0],X[1])
    plt.scatter(X_shift[0],X_shift[1])
    plt.xlabel("x",fontsize=16)
    plt.xticks(fontsize = 15)
    plt.yticks(fontsize = 15)
    plt.ylabel("y",fontsize=16)
    plt.title("PCA on 4 points",fontsize=16)
    plt.show()
    pass

def PCA_flower(X, reducedDim):   
    mean_sepal_length = np.mean(X['sepal length'])
    mean_sepal_width = np.mean(X['sepal width'])
    mean_petal_length = np.mean(X['petal length'])
    mean_petal_width = np.mean(X['petal width'])
  
    X['sepal length'] = X['sepal length']-mean_sepal_length
    X['sepal width'] = X['sepal width']-mean_sepal_width
    X['petal length'] = X['petal length']-mean_petal_length
    X['petal width'] = X['petal width']-mean_petal_width
    
    print(mean_sepal_length)
    print(mean_sepal_width)
    # sb.scatterplot(data= X,hue="species",y="sepal width",x="petal width")
    # plt.xlim(-4,4)
    # plt.ylim(-1.5,1.5)
    # plt.show()
    
    X_shift = X
    return X_shift
    pass

def SVM(X):
    print(X)
    y =  []
    
    for i in range (len(X)) :
        if X["species"][i] == "interior":
            y.append(1)
        else:
            y.append(-1)
        pass
    print(y)
    
    clf = svm.SVC(kernel='linear',C=1E10)
    clf.fit(X.iloc[:,0:2],y)
    
    new_data = np.array([[5.1,7.0,6.4,6.3,5.8,4.9],[3.5,3.2,3.2,3.3,2.7,3.0]])
    
    plt.scatter(new_data[0]-5.84,new_data[1]-3.05,c="r",marker="*",s=90)
    sb.scatterplot(data= X,hue="species",x="sepal length",y="sepal width")
    # plt.scatter(X["sepal length"], X["sepal width"], c=y, s=30, cmap=plt.cm.Paired)
    
    ax = plt.gca()
    xlim = ax.get_xlim()
    ylim = ax.get_ylim()
    
    xx = np.linspace(xlim[0], xlim[1], 30)
    yy = np.linspace(ylim[0], ylim[1], 30)
    YY, XX = np.meshgrid(yy, xx)
    xy = np.vstack([XX.ravel(), YY.ravel()]).T
    Z = clf.decision_function(xy).reshape(XX.shape)
    
    ax.contour(
    XX, YY, Z, colors="k", levels=[-1, 0, 1], alpha=0.5, linestyles=["--", "-", "--"]
)
    
    ax.scatter(
    clf.support_vectors_[:, 0],
    clf.support_vectors_[:, 1],
    s=100,
    linewidth=1,
    facecolors="none",
    edgecolors="k",
)
    plt.xlabel("Sepal Length",fontsize=16)
    plt.xticks(fontsize = 15)
    plt.yticks(fontsize = 15)
    plt.ylabel("Sepal Width",fontsize=16)
    plt.title("SVM on Flower Dataset",fontsize=16)
    plt.show()
    
     
    pass

def main():
    x,data= feel_data()

    X = [[1,5,3,3],[4,4,3,5]]
    X_two = [[1.268,4.732,3.5,2.5],[3,5,3.134,4.866]]
    
    PCA(X_two,None)
    # There is a rotation between X and X two
    
    X_shift = PCA_flower(data,None)
    
    SVM(X_shift)
    
    pass

if __name__ == "__main__" :
    main()